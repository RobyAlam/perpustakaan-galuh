import React, { useState, useEffect } from 'react';
import GridContainer from '../../../@jumbo/components/GridContainer';
import PageContainer from '../../../@jumbo/components/PageComponents/layouts/PageContainer';

import Grid from '@material-ui/core/Grid';
import Collapse from '@material-ui/core/Collapse';
import Button from '@material-ui/core/Button';
import TablePagination from '@material-ui/core/TablePagination';
import TableContainer from '@material-ui/core/TableContainer';
import makeStyles from '@material-ui/core/styles/makeStyles';
import { fade } from '@material-ui/core';
import Box from '@material-ui/core/Box';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';

import CmtCard from '@coremat/CmtCard';
import CmtCardHeader from '@coremat/CmtCard/CmtCardHeader';
import CmtSearch from '@coremat/CmtSearch';
import CmtCardContent from '@coremat/CmtCard/CmtCardContent';
import ListEmptyResult from '@coremat/CmtList/ListEmptyResult';

import axios from 'axios';

import { PEMINJAMAN, PENGEMBALIAN } from 'Constant';

import Table from './Table';
import Create from './Create';
import Detail from './Detail';

const useStyles = makeStyles(theme => ({
    headerRoot: {
        paddingBottom: 10,
        paddingTop: 10,
        position: 'relative',
        [theme.breakpoints.down('xs')]: {
            '&.Cmt-header-root': {
                flexDirection: 'column',
            },
            '& .Cmt-action-default-menu': {
                position: 'absolute',
                right: 24,
                top: 5,
            },
        },
    },
    cardContentRoot: {
        padding: '0 !important',
        borderTop: `solid 1px ${theme.palette.borderColor.main}`,
        marginTop: -1,
    },
    scrollbarRoot: {
        height: 590,
        '& .CmtList-EmptyResult': {
            backgroundColor: 'transparent',
            border: '0 none',
        },
    },
    searchAction: {
        position: 'relative',
        width: 38,
        height: 38,
    },
    searchActionBar: {
        position: 'absolute',
        right: 0,
        top: 2,
        zIndex: 1,
    },
    newsListRoot: {
        padding: 24,
        cursor: 'pointer',
        transition: 'all .2s',
        '&:not(:first-child)': {
            borderTop: `solid 1px ${theme.palette.borderColor.main}`,
        },
        '& .Cmt-media-object': {
            width: '100%',
        },
        '& .fav-btn': {
            transform: 'scale(0)',
            transition: 'all .2s',
        },
        '&:hover': {
            backgroundColor: fade(theme.palette.primary.main, 0.1),
            transform: 'translateY(-4px)',
            boxShadow: `0 3px 10px 0 ${fade(theme.palette.common.dark, 0.2)}`,
            '& .fav-btn': {
                transform: 'scale(1)',
            },
        },
        [theme.breakpoints.down('xs')]: {
            '& .Cmt-media-object': {
                flexDirection: 'column',
            },
            '& .Cmt-media-image': {
                width: '100%',
                alignSelf: 'normal',
                marginBottom: 10,
                '& img': {
                    marginRight: 0,
                    width: '100%',
                },
            },
        },
    },
    titleRoot: {
        [theme.breakpoints.down('sm')]: {
            paddingTop: 16,
        },
    },
    table: {
        minWidth: 500,
    },
}));

const breadcrumbs = [
    { label: 'Home', link: '/' },
    { label: 'Daftar Pengembalian', isActive: true },
];

const PengembalianPage = () => {
    const classes = useStyles();
    const [isOpen, setIsOpen] = useState(false);
    const [filterIsOpen, setFilterIsOpen] = useState(false);
    const [listPengembalian, setListPengembalian] = useState([]);
    const [page, setPage] = useState(0);
    const [rowsPerPage, setRowsPerPage] = useState(10);
    const [selectedData, setSelectedData] = useState(null);
    const [search, setSearch] = useState('');
    const [tglPinjamDari, setTglPinjamDari] = useState('');
    const [tglPinjamKe, setTglPinjamKe] = useState('');
    const [tglKembaliDari, setTglKembaliDari] = useState('');
    const [tglKembaliKe, setTglKembaliKe] = useState('');

    const loadData = () => {
        let query_url = `?page=${page}&perPage=${rowsPerPage}&search=${search}`;
        axios.get(PENGEMBALIAN + query_url).then(
            res => {
                if (res.status === 200) {
                    setListPengembalian(res.data.list.data);
                }
            }
        ).catch(
            error => {
                console.log(error)
                alert("Terjadi kesalahan saat fetch data.")
            }
        )
    }

    useEffect(() => {
        loadData()
    }, [search, page, rowsPerPage])

    const onBackClick = () => setSelectedData(null);

    const onChangeSearch = e => {
        if (e.keyCode === 13) {
            setSearch(e.target.value)
        }
    }


    return (
        <PageContainer heading="Daftar Pengembalian" breadcrumbs={breadcrumbs}>
            <GridContainer>
                <Grid item xs={12}>
                    <Button variant="contained" color="primary" onClick={() => setIsOpen(true)}>
                        Tambah Data Pengembalian
                    </Button>
                </Grid>
                <Grid item xs={12}>
                        <CmtCard>
                            <CmtCardHeader
                                className={classes.headerRoot}
                                title={
                                    <Box display="flex" alignItems={{ md: 'center' }} flexDirection={{ xs: 'column', md: 'row' }}>
                                        <Typography component="div" variant="h4" className={classes.titleRoot}>
                                            Daftar Pengembalian
                                        </Typography>
                                    </Box>
                                }
                                actionsPos="top-corner">

                            </CmtCardHeader>
                            <CmtCardContent className={classes.cardContentRoot}>
                                <TableContainer component={Paper} className={classes.root}>
                                    <Table
                                        dataList={listPengembalian}
                                        selectedData={selectedData}
                                        setSelectedData={setSelectedData}
                                    />
                                    <TablePagination
                                        rowsPerPageOptions={[10, 25, 50]}
                                        component="div"
                                        count={listPengembalian?.length}
                                        rowsPerPage={rowsPerPage}
                                        page={page}
                                        onChangePage={(e) => {
                                            setPage(e.target.value)
                                        }}
                                        onChangeRowsPerPage={(e) => {
                                            setRowsPerPage(e.target.value)
                                            setPage(0)
                                        }}
                                    />

                                    {listPengembalian?.count === 0 && (
                                        <ListEmptyResult title="No Result" content="No result found with your search" />
                                    )}
                                </TableContainer>
                            </CmtCardContent>
                        </CmtCard>
                </Grid>
            </GridContainer>

            <Create isOpen={isOpen} setIsOpen={setIsOpen} loadData={loadData} />
        </PageContainer>
    );
};

export default PengembalianPage;
