import React, { useState, useEffect } from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import GridContainer from '@jumbo/components/GridContainer';
import Grid from '@material-ui/core/Grid';
import { Box } from '@material-ui/core';
import axios from 'axios';

import { PENGEMBALIAN_INSERT } from 'Constant';

import List from './List';

const CreatePeminjaman = ({ isOpen, setIsOpen, loadData }) => {
    const [peminjam, setPeminjam] = useState([]);
    const [selectedBook, setSelectedBook] = useState([]);
    const [nama, setNama] = useState('');
    const [email, setEmail] = useState('');
    const [noTelp, setNoTelp] = useState('');
    const [alamat, setAlamat] = useState('');
    const [tglKembali, setTglKembali] = useState('');

    const handleClose = () => {
        setIsOpen(false);
    }

    const onSubmit = () => {
        if (selectedBook.length > 0) {
            let data = {
                list: selectedBook
            }
            axios.post(PENGEMBALIAN_INSERT, data).then(
                res => {
                    alert(res.data.message)
                    window.location.reload()
                }
            )
        } else {
            alert("Harap pilih minimal 1 peminjaman.")
        }
    }


    return (
        <Box>
            <Dialog open={isOpen} onClose={handleClose} aria-labelledby="form-dialog-title" maxWidth='md' fullWidth={true}>
                <DialogTitle id="form-dialog-title">Form Pengembalian</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        Pilih Data Peminjaman.
                    </DialogContentText>
                    <GridContainer>
                        <Grid item xs={12}>
                            <List
                                setSelectedBook={setSelectedBook}
                                selectedBook={selectedBook}
                            />
                        </Grid>
                    </GridContainer>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose} color="primary">
                        Cancel
                    </Button>
                    <Button onClick={onSubmit} color="primary">
                        Submit
                    </Button>
                </DialogActions>
            </Dialog>
        </Box>
    )
}

export default CreatePeminjaman;