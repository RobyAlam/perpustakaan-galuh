import React, { useState, useEffect } from 'react';
import GridContainer from '../../../@jumbo/components/GridContainer';
import PageContainer from '../../../@jumbo/components/PageComponents/layouts/PageContainer';
import Box from '@material-ui/core/Box';
import IntlMessages from '../../../@jumbo/utils/IntlMessages';
import Grid from '@material-ui/core/Grid';
import TablePagination from '@material-ui/core/TablePagination';
import TableContainer from '@material-ui/core/TableContainer';
import Paper from '@material-ui/core/Paper';
import { lighten, makeStyles, useTheme } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import TableHead from '@material-ui/core/TableHead';
import { fade } from '@material-ui/core';
import axios from 'axios';
import CmtCard from '@coremat/CmtCard';
import CmtCardHeader from '@coremat/CmtCard/CmtCardHeader';
import CmtSearch from '@coremat/CmtSearch';
import CmtCardContent from '@coremat/CmtCard/CmtCardContent';
import ListEmptyResult from '@coremat/CmtList/ListEmptyResult';
import Typography from '@material-ui/core/Typography';

import { PEMINJAM } from 'Constant';

const breadcrumbs = [
    { label: 'Home', link: '/' },
    { label: 'Peminjam', isActive: true },
];

const useStyles = makeStyles(theme => ({
    headerRoot: {
        paddingBottom: 10,
        paddingTop: 10,
        position: 'relative',
        [theme.breakpoints.down('xs')]: {
            '&.Cmt-header-root': {
                flexDirection: 'column',
            },
            '& .Cmt-action-default-menu': {
                position: 'absolute',
                right: 24,
                top: 5,
            },
        },
    },
    cardContentRoot: {
        padding: '0 !important',
        borderTop: `solid 1px ${theme.palette.borderColor.main}`,
        marginTop: -1,
    },
    scrollbarRoot: {
        height: 590,
        '& .CmtList-EmptyResult': {
            backgroundColor: 'transparent',
            border: '0 none',
        },
    },
    searchAction: {
        position: 'relative',
        width: 38,
        height: 38,
    },
    searchActionBar: {
        position: 'absolute',
        right: 0,
        top: 2,
        zIndex: 1,
    },
    newsListRoot: {
        padding: 24,
        cursor: 'pointer',
        transition: 'all .2s',
        '&:not(:first-child)': {
            borderTop: `solid 1px ${theme.palette.borderColor.main}`,
        },
        '& .Cmt-media-object': {
            width: '100%',
        },
        '& .fav-btn': {
            transform: 'scale(0)',
            transition: 'all .2s',
        },
        '&:hover': {
            backgroundColor: fade(theme.palette.primary.main, 0.1),
            transform: 'translateY(-4px)',
            boxShadow: `0 3px 10px 0 ${fade(theme.palette.common.dark, 0.2)}`,
            '& .fav-btn': {
                transform: 'scale(1)',
            },
        },
        [theme.breakpoints.down('xs')]: {
            '& .Cmt-media-object': {
                flexDirection: 'column',
            },
            '& .Cmt-media-image': {
                width: '100%',
                alignSelf: 'normal',
                marginBottom: 10,
                '& img': {
                    marginRight: 0,
                    width: '100%',
                },
            },
        },
    },
    titleRoot: {
        [theme.breakpoints.down('sm')]: {
            paddingTop: 16,
        },
    },
    table: {
        minWidth: 500,
    },
}));

const PeminjamPage = () => {
    const classes = useStyles();
    const [peminjam, setPeminjam] = useState([]);
    const [total, setTotal] = useState(0);
    const [page, setPage] = useState(0);
    const [rowsPerPage, setRowsPerPage] = useState(10);
    const [search, setSearch] = useState('');

    useEffect(() => {
        let query_url = `?page=${page}&perPage=${rowsPerPage}&search=${search}`
        axios.get(PEMINJAM + query_url).then(
            res => {
                if (res.status === 200) {
                    setPeminjam(res.data.data.data);
                    setTotal(res.data.data.total)
                }
            }
        ).catch(
            error => {
                console.log(error)
                alert("Terjadi kesalahan")
            }
        )
    }, [search, page, rowsPerPage]);

    const onChangeSearch = e => {
        if (e.keyCode === 13) {
            setSearch(e.target.value)
        }
    }

    return (
        <PageContainer heading="Daftar Peminjam" breadcrumbs={breadcrumbs}>
            <CmtCard>
                <CmtCardHeader
                    className={classes.headerRoot}
                    title={
                        <Box display="flex" alignItems={{ md: 'center' }} flexDirection={{ xs: 'column', md: 'row' }}>
                            <Typography component="div" variant="h4" className={classes.titleRoot}>
                                Daftar Peminjam
                                        </Typography>
                        </Box>
                    }
                    actionsPos="top-corner">
                    <Box className={classes.searchAction}>
                        <Box className={classes.searchActionBar}>
                            <CmtSearch onlyIcon border={false} onKeyUp={onChangeSearch} />
                        </Box>
                    </Box>
                </CmtCardHeader>
                <CmtCardContent className={classes.cardContentRoot}>
                    <TableContainer component={Paper} >
                        <Table aria-label="custom pagination table">
                            <TableHead>
                                <TableRow>
                                    <TableCell align="left" style={{ minWidth: 50 }}>
                                        No
                            </TableCell>
                                    <TableCell align="left" style={{ minWidth: 100 }}>
                                        Nama
                            </TableCell>
                                    <TableCell align="left" style={{ minWidth: 100 }}>
                                        Email
                            </TableCell>
                                    <TableCell align="left" style={{ minWidth: 100 }}>
                                        Nomor Telepon
                            </TableCell>
                                    <TableCell align="left" style={{ minWidth: 100 }}>
                                        Alamat
                            </TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {peminjam?.map((val, i) => {
                                    return (
                                        <TableRow key={i} hover>
                                            <TableCell>
                                                {i + 1}
                                            </TableCell>
                                            <TableCell>
                                                {val.nama}
                                            </TableCell>
                                            <TableCell>
                                                {val.email}
                                            </TableCell>
                                            <TableCell>
                                                {val.no_telp}
                                            </TableCell>
                                            <TableCell>
                                                {val.alamat.substring(0, 50)}...
                                            </TableCell>
                                        </TableRow>
                                    )
                                })}
                            </TableBody>
                        </Table>
                        <TablePagination
                            rowsPerPageOptions={[10, 25, 50]}
                            component="div"
                            count={total}
                            rowsPerPage={rowsPerPage}
                            page={page}
                            onChangePage={(e) => {
                                setPage(e.target.value)
                            }}
                            onChangeRowsPerPage={(e) => {
                                setRowsPerPage(e.target.value)
                                setPage(0)
                            }}
                        />
                    </TableContainer>
                </CmtCardContent>
            </CmtCard>
        </PageContainer>
    );
};

export default PeminjamPage;