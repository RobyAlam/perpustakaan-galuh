import React, { useState, useEffect } from 'react';
import Box from '@material-ui/core/Box';
import IconButton from '@material-ui/core/IconButton';
import { Close } from '@material-ui/icons';
import Tooltip from '@material-ui/core/Tooltip';
import CmtCard from '@coremat/CmtCard';
import Typography from '@material-ui/core/Typography';
import makeStyles from '@material-ui/core/styles/makeStyles';
import GridContainer from '@jumbo/components/GridContainer';
import Grid from '@material-ui/core/Grid';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import Divider from '@material-ui/core/Divider';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';

import axios from 'axios';

import { PEMINJAMAN } from 'Constant';

const useStyles = makeStyles(theme => ({
    root: {
        width: '100%',
    },
    titleRoot: {
        fontSize: 16,
        marginLeft: 12,
        fontWeight: theme.typography.fontWeightBold,
    },
    tagListRoot: {
        color: theme.palette.text.disabled,
        padding: '0 3px 0 0',
        letterSpacing: 0.4,
        fontSize: 12,
        width: 'auto',
    },
    blockRoot: {
        display: 'block',
        color: theme.palette.text.disabled,
    },
    descriptionBlock: {
        '& p': {
            marginBottom: 16,
            fontSize: 14,
            color: theme.palette.text.secondary,
        },
    },
    linkBtn: {
        cursor: 'pointer',
        textTransform: 'uppercase',
    },
    imageRoot: {
        width: '100%',
        height: 250,
    },
    badge: {
        position: 'absolute',
        bottom: 15,
        left: 20,
        zIndex: 1,
        fontSize: 12,
        padding: '4px 16px',
        letterSpacing: 0.4,
        borderRadius: 16,
        backgroundColor: theme.palette.common.black,
        color: theme.palette.common.white,
    },
    inline: {
        display: 'inline',
    },
    large: {
        width: theme.spacing(18),
        height: theme.spacing(20),
        marginRight: theme.spacing(2)
    }
}));

const reformatDate = (date) => {
    const months = [
        "Januari",
        "Februari",
        "Maret",
        "April",
        "Mei",
        "Juni",
        "Juli",
        "Agustus",
        "September",
        "Oktober",
        "November",
        "Desember",
    ];
    const d = new Date(date);

    return d.getDate() + " " + months[d.getMonth()] + " " + d.getFullYear();
};


const Detail = ({
    selectedData,
    onBackClick
}) => {
    const classes = useStyles();
    const [peminjaman, setPeminjaman] = useState([]);
    const [peminjam, setPeminjam] = useState([]);
    const [buku, setBuku] = useState([]);

    useEffect(() => {
        axios.get(PEMINJAMAN + `/${selectedData}`).then(
            res => {
                if (res.data.status === 200) {
                    setPeminjaman(res.data.peminjaman);
                    setPeminjam(res.data.peminjam);
                    setBuku(res.data.detail);
                }
            }
        )
    }, [])

    return (
        <CmtCard>
            <Box display="flex" flexDirection={{ xs: 'column', sm: 'row' }} alignItems={{ sm: 'center' }} px={6} py={3}>
                <Box display="flex" alignItems="center" mb={{ xs: 2, sm: 0 }}>
                    <Tooltip title="close">
                        <Box ml={-3} clone>
                            <IconButton onClick={onBackClick}>
                                <Close />
                            </IconButton>
                        </Box>
                    </Tooltip>
                    <Typography component="div" variant="h4" className={classes.titleRoot}>
                        Peminjaman #PN-{peminjaman.id}
                    </Typography>
                </Box>
            </Box>
            <Box p={6}>
                <GridContainer>
                    <Grid item xs={6}>
                        <Typography variant="h4">Tanggal Pinjam: {reformatDate(peminjaman.created_at)}</Typography>
                    </Grid>
                    <Grid item xs={6}>
                        <Typography variant="h4">Tanggal Kembali: {reformatDate(peminjaman.tgl_kembali)}</Typography>
                    </Grid>
                    <Grid item xs={6}>
                        <Typography variant="h4">Nama Peminjam: {peminjam.nama}</Typography>
                    </Grid>
                    <Grid item xs={6}>
                        <Typography variant="h4">Nomor Telepon: {peminjam.no_telp}</Typography>
                    </Grid>
                    <Grid item xs={6}>
                        <Typography variant="h4">Email: {peminjam.email}</Typography>
                    </Grid>
                    <Grid item xs={12}>
                        <Typography variant="h4">Alamat: </Typography>
                        <Typography variant="p">{peminjam.alamat}</Typography>
                    </Grid>
                    <Grid item xs={12}>
                        <Divider />
                        <List className={classes.root}>
                            {buku?.map((val, i) => {
                                return (
                                    <>
                                        <ListItem alignItem="flex-start">
                                            <ListItemAvatar>
                                                <Avatar alt="cek" src={'http://localhost:8000/assets/buku/1615540185_photo6174509525659396666.jpg'} variant="square" className={classes.large} />
                                            </ListItemAvatar>
                                            <ListItemText
                                                primary={val.judul}
                                                secondary={
                                                    <React.Fragment>
                                                        <Typography component="span" variant="body2" className={classes.inline} color="textPrimary">
                                                            {val.penulis} | {val.penerbit}
                                                        </Typography>
                                                        {" - " + val.deskripsi.substring(0, 150)}...
                                                    </React.Fragment>
                                                }
                                            />
                                        </ListItem>
                                        <Divider variant="inset" component="li" />
                                    </>
                                )
                            })}
                        </List>
                    </Grid>
                </GridContainer>
            </Box>
        </CmtCard>
    )
}

export default Detail;