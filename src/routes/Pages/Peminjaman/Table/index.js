import React, { useEffect, useState } from 'react';
import { lighten, makeStyles, useTheme } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import TableHead from '@material-ui/core/TableHead';


const useStyles = makeStyles(theme => ({
    root: {
        backgroundColor: lighten(theme.palette.background.paper, 0.1),
    },
    table: {
        minWidth: 500,
    },
}));

const reformatDate = (date) => {
  const months = [
    "Januari",
    "Februari",
    "Maret",
    "April",
    "Mei",
    "Juni",
    "Juli",
    "Agustus",
    "September",
    "Oktober",
    "November",
    "Desember",
  ];
  const d = new Date(date);

  return d.getDate() + " " + months[d.getMonth()] + " " + d.getFullYear();
};

const TabelPeminjaman = ({
    dataList,
    selectedData,
    setSelectedData
}) => {
    const classes = useStyles();

    return (
            <Table className={classes.table} aria-label="custom pagination table">
                <TableHead>
                    <TableRow>
                        <TableCell align="left" style={{ minWidth: 50 }}>
                            No
                        </TableCell>
                        <TableCell align="left" style={{ minWidth: 100 }}>
                            Kode Peminjaman
                        </TableCell>
                        <TableCell align="left" style={{ minWidth: 100 }}>
                            Nama Peminjam
                        </TableCell>
                        <TableCell align="left" style={{ minWidth: 100 }}>
                            Nomor Telepon
                        </TableCell>
                        <TableCell align="left" style={{ minWidth: 100 }}>
                            Tanggal Pinjam
                        </TableCell>
                        <TableCell align="left" style={{ minWidth: 100 }}>
                            Tanggal Kembali
                        </TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {dataList?.map((val, i) => {
                        return (
                            <TableRow hover key={i}>
                                <TableCell>
                                    {i+1}
                                </TableCell>
                                <TableCell>
                                    <a href='#' onClick={() => setSelectedData(val.id)}>PN-{val.id}</a>
                                </TableCell>
                                <TableCell>
                                    {val.nama}
                                </TableCell>
                                <TableCell>
                                    {val.no_telp}
                                </TableCell>
                                <TableCell>
                                    {reformatDate(val.created_at)}
                                </TableCell>
                                <TableCell>
                                    {reformatDate(val.tgl_kembali)}
                                </TableCell>
                            </TableRow>
                        )
                    })}

                </TableBody>
            </Table>
    )
}

export default TabelPeminjaman;