import React, { useState, useEffect } from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import GridContainer from '@jumbo/components/GridContainer';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { Box } from '@material-ui/core';

import axios from 'axios';


const Filter = ({ filterIsOpen, setFilterIsOpen, setTglPinjamDari, setTglPinjamKe, setTglKembaliDari, setTglKembaliKe, onFilterApply }) => {

    const handleClose = () => {
        setFilterIsOpen(false)
    }

    return (
        <Box>
            <Dialog open={filterIsOpen} onClose={handleClose} aria-labelledby="form-dialog-title" fullWidth={true} maxWidth='xs'>
                <DialogTitle id="form-dialog-title">Filter</DialogTitle>
                <DialogContent>
                    <GridContainer>
                        <Grid item xs='12'>
                            <Typography variant='h5'>
                                Tanggal Pinjam
                            </Typography>
                        </Grid>
                        <Grid item xs='6'>
                            <TextField
                                id="date"
                                label="Dari"
                                type="date"
                                InputLabelProps={{
                                    shrink: true,
                                }}
                                onChange={e => setTglPinjamDari(e.target.value)}
                            />
                        </Grid>
                        <Grid item xs='6'>
                            <TextField
                                id="date"
                                label="Sampai"
                                type="date"
                                InputLabelProps={{
                                    shrink: true,
                                }}
                                onChange={e => setTglPinjamKe(e.target.value)}
                            />
                        </Grid>
                        <Grid item xs='12'>
                            <Typography variant='h5'>
                                Tanggal Kembali
                            </Typography>
                        </Grid>
                        <Grid item xs='6'>
                            <TextField
                                id="date"
                                label="Dari"
                                type="date"
                                InputLabelProps={{
                                    shrink: true,
                                }}
                                onChange={e => setTglKembaliDari(e.target.value)}
                            />
                        </Grid>
                        <Grid item xs='6'>
                            <TextField
                                id="date"
                                label="Sampai"
                                type="date"
                                InputLabelProps={{
                                    shrink: true,
                                }}
                                onChange={e => setTglKembaliKe(e.target.value)}
                            />
                        </Grid>
                    </GridContainer>
                </DialogContent>
                <DialogActions>
                    <Button color="primary" onClick={handleClose}>
                        Cancel
                    </Button>
                    <Button color="primary" onClick={onFilterApply}>
                        Submit
                    </Button>
                </DialogActions>
            </Dialog>
        </Box>
    )
}

export default Filter;