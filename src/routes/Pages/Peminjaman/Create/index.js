import React, { useState, useEffect } from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import GridContainer from '@jumbo/components/GridContainer';
import Grid from '@material-ui/core/Grid';
import { Box } from '@material-ui/core';
import axios from 'axios';

import { PEMINJAMAN_INSERT } from 'Constant';

import List from './List';

const CreatePeminjaman = ({ isOpen, setIsOpen, loadData }) => {
    const [peminjam, setPeminjam] = useState([]);
    const [selectedBook, setSelectedBook] = useState([]);
    const [nama, setNama] = useState('');
    const [email, setEmail] = useState('');
    const [noTelp, setNoTelp] = useState('');
    const [alamat, setAlamat] = useState('');
    const [tglKembali, setTglKembali] = useState('');

    const handleClose = () => {
        setIsOpen(false);
    }

    const onSubmit = () => {
        if (selectedBook.length > 0) {
            if (nama && email && noTelp && alamat) {
                let data = {
                    tgl_kembali: tglKembali,
                    nama: nama,
                    email: email,
                    no_telp: noTelp,
                    alamat: alamat,
                    buku: selectedBook
                }
                axios.post(PEMINJAMAN_INSERT, data).then(
                    res => {
                        alert(res.data.message);
                    }
                ).catch(
                    error => {
                        console.log(error);
                        alert("Terjadi kesalahan")
                    }
                )
                loadData(1, 10)
                clearForm()
            } else {
                alert("Mohon isi semua form yang disediakan");
            }
        } else {
            alert("Harap pilih minimal 1 buku.")
        }
    }

    const clearForm = () => {
        setTglKembali('')
        setNama('')
        setEmail('')
        setNoTelp('')
        setAlamat('')
        setSelectedBook([])
        handleClose()
    }

    return (
        <Box>
            <Dialog open={isOpen} onClose={handleClose} aria-labelledby="form-dialog-title" maxWidth='md' fullWidth={true}>
                <DialogTitle id="form-dialog-title">Input Data Peminjaman</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        Harap isi semua form yang disediakan sebelum submit.
                    </DialogContentText>
                    <GridContainer>
                        <Grid item xs={8}>
                            <List
                                setSelectedBook={setSelectedBook}
                                selectedBook={selectedBook}
                            />
                        </Grid>
                        <Grid item xs={4}>
                            {selectedBook.length > 0 && (
                                <>
                                    <label>Buku yang dipilih:</label><br />
                                    <label>
                                        {selectedBook?.map((val, i) => {
                                            var selected = val.judul + ', ';
                                            if (i + 1 === selectedBook.length) {
                                                selected = val.judul + '.'
                                            }
                                            return (
                                                <>
                                                    {selected}
                                                </>
                                            )
                                        })}
                                    </label><br />
                                </>
                            )}
                            <label>Harap isi data peminjam dibawah</label>
                            <TextField autoFocus margin="dense" id="tgl_kembali" label="Tanggal Kembali" type="date" fullWidth onChange={e => setTglKembali(e.target.value)} />
                            <TextField margin="dense" id="nama" label="Nama Peminjam" type="text" fullWidth onChange={e => setNama(e.target.value)} />
                            <TextField margin="dense" id="email" label="Email" type="text" fullWidth onChange={e => setEmail(e.target.value)} />
                            <TextField margin="dense" id="no_telp" label="Nomor Telepon" type="text" fullWidth onChange={e => setNoTelp(e.target.value)} />
                            <TextField multiline rows={3} margin="dense" id="alamat" label="Alamat" type="text" fullWidth onChange={e => setAlamat(e.target.value)} />
                        </Grid>
                    </GridContainer>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose} color="primary">
                        Cancel
                    </Button>
                    <Button onClick={onSubmit} color="primary">
                        Submit
                    </Button>
                </DialogActions>
            </Dialog>
        </Box>
    )
}

export default CreatePeminjaman;