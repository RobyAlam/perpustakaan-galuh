import React, { useEffect, useState } from 'react';
import { lighten, makeStyles, useTheme } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableFooter from '@material-ui/core/TableFooter';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import Checkbox from '@material-ui/core/Checkbox';
import Paper from '@material-ui/core/Paper';
import IconButton from '@material-ui/core/IconButton';
import FirstPageIcon from '@material-ui/icons/FirstPage';
import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft';
import TableHead from '@material-ui/core/TableHead';

import ListEmptyResult from '@coremat/CmtList/ListEmptyResult';
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';
import LastPageIcon from '@material-ui/icons/LastPage';

import { BOOKS } from 'Constant';
import axios from 'axios';

const useStyles = makeStyles(theme => ({
    root: {
        backgroundColor: lighten(theme.palette.background.paper, 0.1),
    },
    table: {
        minWidth: 500,
    },
}));

const ListBook = ({
    setSelectedBook,
    selectedBook
}) => {
    const [books, setBooks] = useState([]);
    const [page, setPage] = useState(0);
    const [rowsPerPage, setRowsPerPage] = useState(10);
    const [total, setTotal] = useState(0);
    const classes = useStyles();

    useEffect(() => {
        axios.get(BOOKS).then(
            res => {
                if (res.status === 200) {
                    setBooks(res.data.buku.data);
                    setTotal(res.data.buku.total);
                }
            }
        )
    }, []);

    const handleClick = (event, item) => {
        const selectedIndex = selectedBook.indexOf(item);
        if (item.jumlah_tersedia > 0) {
            let newSelected = [];

            if (selectedIndex === -1) {
                newSelected = newSelected.concat(selectedBook, item);
            } else if (selectedIndex === 0) {
                newSelected = newSelected.concat(selectedBook.slice(1));
            } else if (selectedIndex === selectedBook.length - 1) {
                newSelected = newSelected.concat(selectedBook.slice(0, -1));
            } else if (selectedIndex > 0) {
                newSelected = newSelected.concat(selectedBook.slice(0, selectedIndex), selectedBook.slice(selectedIndex + 1));
            }

            setSelectedBook(newSelected);
        }
    };

    const isSelected = item => selectedBook.indexOf(item) !== -1;


    return (
        <TableContainer component={Paper} className={classes.root}>
            <Table className={classes.table} aria-label="custom pagination table">
                <TableHead>
                    <TableRow>
                        <TableCell align="left" style={{ minWidth: 10 }}>
                            No
                        </TableCell>
                        <TableCell align="left" style={{ minWidth: 10 }} />
                        <TableCell align="left" style={{ minWidth: 100 }}>
                            Judul
                        </TableCell>
                        <TableCell align="left" style={{ minWidth: 20 }}>
                            Jumlah Tersedia
                        </TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {books?.map((val, i) => {
                        const isItemSelected = isSelected(val);
                        const labelId = `enhanced-table-checkbox-${i}`;
                        return (
                            <TableRow
                                hover
                                role="checkbox"
                                tabIndex={-1}
                                key={i}
                                aria-checked={isItemSelected}
                                onClick={(event) => handleClick(event, val)}>
                                <TableCell>
                                    {i + 1}
                                </TableCell>
                                <TableCell padding="checkbox">
                                    <Checkbox checked={isItemSelected} inputProps={{ 'aria-labelledby': labelId }} />
                                </TableCell>
                                <TableCell>
                                    {val.judul}
                                </TableCell>
                                <TableCell>
                                    {val.jumlah_tersedia}
                                </TableCell>
                            </TableRow>
                        )
                    })}
                </TableBody>
            </Table>
            <TablePagination
                rowsPerPageOptions={[5, 10, 25]}
                component="div"
                count={total}
                rowsPerPage={rowsPerPage}
                page={page}
                onChangePage={(e) => setPage(e.target.value)}
                onChangeRowsPerPage={(e) => {
                    setRowsPerPage(e.target.value)
                    setPage(0)
                }}
            />
        </TableContainer>
    )
}

export default ListBook;