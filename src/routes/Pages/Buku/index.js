import React from 'react';
import GridContainer from '@jumbo/components/GridContainer';
import Grid from '@material-ui/core/Grid';
import PageContainer from '@jumbo/components/PageComponents/layouts/PageContainer';
import { MuiComponentDemo } from '@jumbo/components/PageComponents';
import Box from '@material-ui/core/Box';
import Card from '@material-ui/core/Card';
import BookList from './BookList';

const breadcrumbs = [
    { label: 'Home', link: '/' },
    { label: 'Buku', isActive: true },
];

const BukuPage = () => {
    return (
        <PageContainer heading="Daftar Buku" breadcrumbs={breadcrumbs}>
            <GridContainer>
                <Grid item xs={12}>
                    <BookList />
                </Grid>
            </GridContainer>
        </PageContainer>
    );
};

export default BukuPage;