import React from 'react';
import Box from '@material-ui/core/Box';
import IconButton from '@material-ui/core/IconButton';
import { Close } from '@material-ui/icons';
import Tooltip from '@material-ui/core/Tooltip';
import { Divider } from '@material-ui/core';
import CmtCard from '@coremat/CmtCard';
import Typography from '@material-ui/core/Typography';
import makeStyles from '@material-ui/core/styles/makeStyles';
import CmtCardMedia from '@coremat/CmtCard/CmtCardMedia';

import { BOOK_ASSETS } from 'Constant';

const useStyles = makeStyles(theme => ({
    titleRoot: {
        fontSize: 16,
        marginLeft: 12,
        fontWeight: theme.typography.fontWeightBold,
    },
    tagListRoot: {
        color: theme.palette.text.disabled,
        padding: '0 3px 0 0',
        letterSpacing: 0.4,
        fontSize: 12,
        width: 'auto',
    },
    blockRoot: {
        display: 'block',
        color: theme.palette.text.disabled,
    },
    descriptionBlock: {
        '& p': {
            marginBottom: 16,
            fontSize: 14,
            color: theme.palette.text.secondary,
        },
    },
    linkBtn: {
        cursor: 'pointer',
        textTransform: 'uppercase',
    },
    imageRoot: {
        width: '100%',
        height: 250,
    },
    badge: {
        position: 'absolute',
        bottom: 15,
        left: 20,
        zIndex: 1,
        fontSize: 12,
        padding: '4px 16px',
        letterSpacing: 0.4,
        borderRadius: 16,
        backgroundColor: theme.palette.common.black,
        color: theme.palette.common.white,
    },
}));

const Detail = ({ selectedBook, onBackClick }) => {
    const classes = useStyles();

    const getSubTitle = () => (
        <Box display="flex" flexWrap="wrap" alignItems="center" color="text.disabled" fontSize={12} mb={3}>
            <Box component="span" mr={2} color="primary.main">
                {selectedBook.penulis}
            </Box>
            <Divider orientation="vertical" flexItem />
            <Box mx={2}>{selectedBook.penerbit} - {selectedBook.tahun_terbit}</Box>
            <Divider orientation="vertical" flexItem />
            <Box ml={2}>
                <Box component="span">Jumlah </Box>
                <Box component="span" color="text.primary" mr={2}>
                    {selectedBook.jumlah}
                </Box>
            </Box>
        </Box>
    )

    return (
        <CmtCard>
            <Box display="flex" flexDirection={{ xs: 'column', sm: 'row' }} alignItems={{ sm: 'center' }} px={6} py={3}>
                <Box display="flex" alignItems="center" mb={{ xs: 2, sm: 0 }}>
                    <Tooltip title="close">
                        <Box ml={-3} clone>
                            <IconButton onClick={onBackClick}>
                                <Close />
                            </IconButton>
                        </Box>
                    </Tooltip>
                    <Typography component="div" variant="h4" className={classes.titleRoot}>
                        {selectedBook.judul}
                    </Typography>
                </Box>
            </Box>
            <Box position="relative">
                <CmtCardMedia className={classes.imageRoot} image={selectedBook.foto === null ? 'https://via.placeholder.com/575x480' : BOOK_ASSETS + selectedBook.foto} title={selectedBook.judul} />
            </Box>
            <Box p={6}>
                {getSubTitle()}
                <Box className={classes.descriptionBlock} dangerouslySetInnerHTML={{ __html: selectedBook.deskripsi }} />
            </Box>
        </CmtCard>
    );
}

export default Detail;