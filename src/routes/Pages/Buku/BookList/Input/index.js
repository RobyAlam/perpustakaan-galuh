import React, { useState, useEffect } from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import GridContainer from '@jumbo/components/GridContainer';
import Grid from '@material-ui/core/Grid';
import { Box } from '@material-ui/core';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import axios from 'axios';
import { makeStyles } from '@material-ui/core/styles';

import { useDispatch } from 'react-redux';
import { fetchError, fetchStart, fetchSuccess } from 'redux/actions/Common';

import { CATEGORIES, BOOK_INSERT } from 'Constant';

const useStyles = makeStyles(theme => ({
    formControl: {
        marginTop: theme.spacing(1),
        minWidth: 200,
    },
}));

const InputForm = ({
    inputForm,
    setInputForm
}) => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const [categories, setCategories] = useState([]);

    const [judul, setJudul] = useState('');
    const [kategori, setKategori] = useState('');
    const [penerbit, setPenerbit] = useState('');
    const [penulis, setPenulis] = useState('');
    const [tahun, setTahun] = useState('');
    const [jumlah, setJumlah] = useState(0);
    const [deskripsi, setDeskripsi] = useState('');
    const [foto, setFoto] = useState(null);

    useEffect(() => {
        axios.get(CATEGORIES).then(
            res => {
                if (res.status === 200) {
                    setCategories(res.data.data);
                }
            }
        ).catch(
            error => {
                console.log(error)
            }
        )
    }, []);

    const handleClose = () => {
        setInputForm(false);
    }

    const handleFileInputChange = e => {
        setFoto(e.target.files[0]);
    }

    const onSubmit = () => {
        if (judul && kategori && penerbit && penulis && tahun && jumlah && deskripsi && foto) {
            let data = new FormData();
            data.append('judul', judul);
            data.append('kategori', kategori);
            data.append('penerbit', penerbit);
            data.append('penulis', penulis);
            data.append('tahun', tahun);
            data.append('jumlah', jumlah);
            data.append('deskripsi', deskripsi);
            data.append('foto', foto);

            dispatch(fetchStart());

            axios.post(BOOK_INSERT, data, {
                headers: {
                    "Content-Type": "multipart/form-data"
                }
            }).then(
                res => {
                    if (res.status === 200) {
                        dispatch(fetchSuccess());
                    } else {
                        dispatch(fetchError(res.data.message));
                    }
                }
            ).catch(
                error => {
                    console.log(error)
                    dispatch(fetchError("Terjadi kesalahan"));
                }
            )
            setInputForm(false);
        } else {
            alert('Harap isi semua data');
        }
    }

    const resetForm = () => {
        setJudul('');
        setKategori('');
        setPenulis('');
        setPenerbit('');
        setJumlah(0);
        setTahun('');
        setDeskripsi('');
        setFoto(null);
    }

    return (
        <Box>
            <Dialog open={inputForm} onClose={handleClose} aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">Input Data Buku</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        Harap isi semua form yang disediakan sebelum submit.
                    </DialogContentText>
                    <GridContainer>
                        <Grid item xs={6}>
                            <TextField autoFocus margin="dense" id="judul" label="Judul" type="text" fullWidth value={judul} onChange={(e) => setJudul(e.target.value)} />
                        </Grid>
                        <Grid item xs={6}>
                            <FormControl className={classes.formControl}>
                                <InputLabel id="demo-simple-select-label">Kategori</InputLabel>
                                <Select labelId="demo-simple-select-label" id="demo-simple-select" value={kategori} onChange={(e) => setKategori(e.target.value)}>
                                    {categories.map((val, i) => {
                                        return (
                                            <MenuItem value={val.id}>{val.nama}</MenuItem>
                                        )
                                    })}
                                </Select>
                            </FormControl>
                        </Grid>
                        <Grid item xs={6}>
                            <TextField margin="dense" id="penulis" label="Penulis" type="text" fullWidth value={penulis} onChange={(e) => setPenulis(e.target.value)} />
                        </Grid>
                        <Grid item xs={6}>
                            <TextField margin="dense" id="penerbit" label="Penerbit" type="text" fullWidth value={penerbit} onChange={(e) => setPenerbit(e.target.value)} />
                        </Grid>
                        <Grid item xs={6}>
                            <TextField margin="dense" id="tahun_terbit" label="Tahun Terbit" type="number" fullWidth value={tahun} onChange={(e) => setTahun(e.target.value)} />
                        </Grid>
                        <Grid item xs={6}>
                            <TextField margin="dense" id="jumlah" label="Jumlah Buku Tersedia" type="number" fullWidth value={jumlah} onChange={(e) => setJumlah(e.target.value)} />
                        </Grid>
                        <Grid item xs={12}>
                            <input accept="image/*" style={{ display: 'none' }} id="contained-button-file" type="file" onChange={(e) => handleFileInputChange(e)} />
                            <label htmlFor="contained-button-file">
                                <Button variant="contained" color="primary" component="span">
                                    Upload
                                </Button>
                            </label><br/>
                            {foto && (
                                <label>Selected: {foto.name}</label>
                            )}
                        </Grid>
                        <Grid item xs={12}>
                            <TextField multiline rows={4} margin="dense" id="deskripsi" label="Deskripsi" type="text" fullWidth value={deskripsi} onChange={(e) => setDeskripsi(e.target.value)} />
                        </Grid>
                    </GridContainer>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose} color="primary">
                        Cancel
                    </Button>
                    <Button onClick={onSubmit} color="primary">
                        Submit
                    </Button>
                </DialogActions>
            </Dialog>
        </Box>
    );
}

export default InputForm;