import React, { useEffect, useState } from 'react';
import Collapse from '@material-ui/core/Collapse';
import axios from 'axios';
import { BOOKS } from 'Constant';

import List from './List';
import Detail from './Detail';
import Input from './Input';

const BookList = () => {
    const [selectedBook, setSelectedBook] = useState(null);
    const [categoryData, setCategoryData] = useState('');
    const [currentTab, setCurrentTab] = useState('');
    const [currentPage, setCurrentPage] = useState(1);
    const [lastPage, setLastPage] = useState('');
    const [searchText, setSearchText] = useState('');
    const [books, setBooks] = useState([]);
    const [categories, setCategories] = useState([]);
    const [inputForm, setInputForm] = useState(false);

    useEffect(() => {
        let query_url = `?page=${currentPage}`;
        if (searchText) {
            query_url += `&searchKey=${searchText}`;
        }
        if (currentTab) {
            query_url += `&kategori=${currentTab}`;
        }
        axios.get(BOOKS + query_url).then(
            res => {
                if (res.status === 200) {
                    setBooks(res.data.buku.data);
                    setCategories(res.data.kategori);
                    setCurrentPage(res.data.buku.current_page);
                    setLastPage(res.data.buku.last_page);
                }
            }
        ).catch(error => {
            console.log(error);
        });
    }, [currentTab, searchText]);

    const onItemClick = item => setSelectedBook(item);
    const onBackClick = () => setSelectedBook(null);

    const onSearchText = e => {
        if (e.keyCode === 13) {
            setSearchText(e.target.value);
        }
    }

    return (
        <React.Fragment>
            <Collapse in={selectedBook} timeout="auto" unmountOnExit>
                {selectedBook && (
                    <Detail selectedBook={selectedBook} onBackClick={onBackClick} />
                )}
            </Collapse>
            <Collapse in={!selectedBook} timeout="auto" unmountOnExit>
                <List
                    currentTab={currentTab}
                    setCurrentTab={setCurrentTab}
                    onItemClick={onItemClick}
                    onBackClick={onBackClick}
                    searchText={searchText}
                    onSearchText={onSearchText}
                    categories={categories}
                    data={books}
                    inputForm={inputForm} 
                    setInputForm={setInputForm}
                />
            </Collapse>

            <Input inputForm={inputForm} setInputForm={setInputForm} />
        </React.Fragment>
    )
}

export default BookList;