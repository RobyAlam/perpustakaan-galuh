import React, { useState, useEffect } from 'react';
import { Redirect, Route, Switch } from 'react-router';
import { useSelector } from 'react-redux';
import { useLocation } from 'react-router-dom';
import Cookies from 'universal-cookie';

import Login from './Auth/Login';
import SamplePage from './Pages/SamplePage';
import BukuPage from './Pages/Buku';
import PeminjamPage from './Pages/Peminjam';
import PeminjamanPage from './Pages/Peminjaman';
import PengembalianPage from './Pages/Pengembalian';
import Error404 from './Pages/404';

const cookies = new Cookies();


const RestrictedRoute = ({ component: Component, ...rest }) => {
    const { authUser } = useSelector(({ auth }) => auth);
    return (
        <Route
            {...rest}
            render={props =>
                authUser ? (
                    <Component {...props} />
                ) : (
                    <Redirect
                        to={{
                            pathname: '/login',
                            state: { from: props.location },
                        }}
                    />
                )
            }
        />
    );
};

const Routes = () => {
    const { authUser } = useSelector(({ auth }) => auth);
    const location = useLocation();

    if (location.pathname === '' || location.pathname === '/') {
        return <Redirect to={'/dashboard'} />;
    } else if (authUser && location.pathname === '/login') {
        return <Redirect to={'/dashboard'} />;
    }

    return (
        <React.Fragment>
            <Switch>
                <Route path='/login' component={Login} />
                <RestrictedRoute path='/dashboard' component={SamplePage} />
                <RestrictedRoute path='/buku' component={BukuPage} />
                <RestrictedRoute path='/peminjam' component={PeminjamPage} />
                <RestrictedRoute path='/peminjaman' component={PeminjamanPage} />
                <RestrictedRoute path='/pengembalian' component={PengembalianPage} />
                <Route component={Error404} />
            </Switch>
        </React.Fragment>
    );
};

export default Routes;
