import React from 'react';
import CmtVertical from '../../../../../@coremat/CmtNavigation/Vertical';
import PerfectScrollbar from 'react-perfect-scrollbar';
import makeStyles from '@material-ui/core/styles/makeStyles';
import { PostAdd, Group, Book } from '@material-ui/icons';

const useStyles = makeStyles(theme => ({
  perfectScrollbarSidebar: {
    height: '100%',
    transition: 'all 0.3s ease',
    '.Cmt-sidebar-fixed &, .Cmt-Drawer-container &': {
      height: 'calc(100% - 167px)',
    },
    '.Cmt-modernLayout &': {
      height: 'calc(100% - 72px)',
    },
    '.Cmt-miniLayout &': {
      height: 'calc(100% - 91px)',
    },
    '.Cmt-miniLayout .Cmt-sidebar-content:hover &': {
      height: 'calc(100% - 167px)',
    },
  },
}));

const SideBar = () => {
  const classes = useStyles();
  const navigationMenus = [
    {
      name: "Main",
      type: 'section',
      children: [
        {
          name: "Dashboard",
          icon: <PostAdd />,
          type: 'item',
          link: '/dashboard',
        },
      ],
    },
    {
      name: "Master",
      type: 'section',
      children: [
        {
          name: "Buku",
          icon: <Book />,
          type: 'item',
          link: '/buku',
        },
        {
            name: "Peminjam",
            icon: <Group />,
            type: 'item',
            link: '/peminjam'
        }
      ]
    },
    {
        name: "Peminjaman",
        type: 'section',
        children: [
            {
                name: "Daftar Peminjaman",
                icon: <Book />,
                type: 'item',
                link: '/peminjaman'
            }
        ]
    },
    {
        name: "Pengembalian",
        type: 'section',
        children: [
            {
                name: "Daftar Pengembalian",
                icon: <Book />,
                type: 'item',
                link: '/pengembalian'
            }
        ]
    }
  ];

  return (
    <PerfectScrollbar className={classes.perfectScrollbarSidebar}>
      <CmtVertical menuItems={navigationMenus} />
    </PerfectScrollbar>
  );
};

export default SideBar;
