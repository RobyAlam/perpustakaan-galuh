const API = "http://localhost:8000/api/";
const ASSETS = "http://localhost:8000/assets/";

export const LOGIN = API + 'login';

export const BOOKS = API + 'buku';
export const BOOK_INSERT = BOOKS + '/insert';
export const BOOK_ASSETS = ASSETS + 'buku/';

export const CATEGORIES = API + 'kategori';

export const PEMINJAM = API + 'peminjam';
export const PEMINJAM_INSERT = PEMINJAM + '/insert';

export const PEMINJAMAN = API + 'peminjaman';
export const PEMINJAMAN_INSERT = PEMINJAMAN + '/insert';

export const PENGEMBALIAN = API + 'pengembalian';
export const PENGEMBALIAN_INSERT = PENGEMBALIAN + '/insert';